import gulp from "gulp";

// config imports
import {path} from "./gulp/config/path.js";

// tasks imports
import {copy} from "./gulp/tasks/copy.js";
import {reset} from "./gulp/tasks/reset.js";
import {html} from "./gulp/tasks/html.js";
import {plugins} from "./gulp/config/plugins.js";
import {server} from "./gulp/tasks/server.js";
import {scss, scssWatch} from "./gulp/tasks/scss.js";
import {js} from "./gulp/tasks/js.js";
import {fonts} from "./gulp/tasks/fonts.js";
import {php} from "./gulp/tasks/php.js";

global.app = {
    path,
    gulp,
    plugins,
};

function watcher() {
    gulp.watch(path.watch.assets, copy);
    gulp.watch(path.watch.html, html);
    gulp.watch(path.watch.php, php);
    gulp.watch(path.watch.scss, scssWatch);
    gulp.watch(path.watch.js, js);
    gulp.watch(path.watch.fonts, fonts);
}

const dev = gulp.series(
    gulp.parallel(copy, fonts, html, scss, js, php),
    gulp.parallel(watcher, server)
);

gulp.task("default", dev);
