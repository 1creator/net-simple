import webpack from "webpack-stream";

export const js = () => {
    return (
        app.gulp
            .src(app.path.src.js, { sourcemaps: true })
            // .pipe(babel({
            //     presets: ['@babel/preset-env']
            //   }))
            // .pipe(concat("app.js"))
            .pipe(
                webpack({
                    mode: "production",
                    optimization: {
                        runtimeChunk: "single",
                        splitChunks: {
                            chunks: "all",
                            maxInitialRequests: Infinity,
                            minSize: 0,
                            cacheGroups: {
                                vendor: {
                                    test: /[\\/]node_modules[\\/]/,
                                    name: 'vendor',
                                },
                            },
                        },
                        minimize: false,
                    },
                    entry: {
                        app: app.path.src.js[0],
                    },
                    output: {
                        filename: "[name].js",
                    },
                })
            )
            .pipe(app.gulp.dest(app.path.build.js))
            .pipe(app.plugins.browserSync.stream())
    );
};
