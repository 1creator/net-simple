import "bootstrap/js/src/modal.js";
import "bootstrap/js/src/dropdown.js";
import "bootstrap/js/src/collapse.js";
import "bootstrap/js/src/button.js";
import "bootstrap/js/src/tab.js";
import {Navigation, Pagination, Swiper} from "swiper";
import PhotoSwipeLightbox from "photoswipe/lightbox";
import PhotoSwipe from "photoswipe";

Swiper.use([Navigation, Pagination]);

function getRandomVersionKey() {
    let keys = Object.keys(VERSIONS);
    return keys[keys.length * Math.random() << 0]
}

function generateContent(version) {
    const header = document.getElementById("header");
    if (version.headerImages) {
        header.style.backgroundImage = `
                linear-gradient(180deg, rgba(44, 40, 48, 0) 44.27%, #2C2830 80%),
            linear-gradient(180deg, rgba(0, 0, 0, 0.6) 0%, rgba(61, 58, 68, 1) 80%),
            url(${version.headerImages})`
    } else {
        header.style.backgroundImage = 'none';
        header.classList.add('header_no-images');
    }
    //duplicate text for tabs
    const arr = ["director", "marketer", "partner"]
    for (const arrKey in arr) {
        if (version.text[arrKey + "Text"] !== "") {
            version.text[arrKey + "TabText"] = version.text[arrKey + "Text"]
        }
        if (version.text[arrKey + "Title"] !== "") {
            version.text[arrKey + "TabTitle"] = version.text[arrKey + "Title"]
        }
    }
    let formIds = Object.keys(version.text).filter(el => el.includes('feedback') || el.includes('form'))
    if (formIds.length > 0) {
        formIds.forEach(id => {
            if (version.text[id] !== "")
                version.text[id + "Modal"] = version.text[id]
        })
    }

    //pasting text in html
    for (const versionKey in version.text) {
        let elem = document.getElementById(versionKey);
        if (elem && version.text[versionKey] !== "") {
            elem.innerHTML = version.text[versionKey]
        }
    }
}

function initVersion() {
    const urlParams = new URLSearchParams(window.location.search);
    let versionKey = urlParams.get('utm_version');
    if (!versionKey) {
        versionKey = localStorage.getItem("utmVersion");
    }
    if (!versionKey) {
        versionKey = getRandomVersionKey()
    }

    if (VERSIONS[versionKey]) {
        console.log('load version: ' + versionKey)
        localStorage.setItem('utmVersion', versionKey);
        generateContent(VERSIONS[versionKey]);
        window.versionKey = versionKey;
    }
}

function sendAnalytics(target) {
    const targetWithVersion = [versionKey, target].join(':');
    console.log(targetWithVersion)
    try {
        window.ym(YM_IDENTIFIER, 'reachGoal', targetWithVersion);
    } catch (e) {
        console.error(e)
    }
    try {
        window.gtag('event', targetWithVersion);
    } catch (e) {
        console.error(e)
    }
}

function sendFormWithXhr(form) {
    let xhr = new XMLHttpRequest();
    xhr.open("POST", form.action);
    let formData = new FormData(form);
    xhr.send(formData);

    if (form.dataset.goal) {
        sendAnalytics(form.dataset.goal)
    }
}

// Валидация форм и отправка целей в метрики
function initForms() {
    const forms = document.querySelectorAll(".needs-validation");
    Array.prototype.slice.call(forms).forEach(function (form) {
        form.addEventListener(
            "submit",
            function (event) {
                event.preventDefault();
                event.stopPropagation();
                const inputs = Array.prototype.slice.call(form.querySelectorAll("input"));
                let invalidParams = [];
                inputs.reduce((acc, i) => {
                    acc[i.name] = i.value
                    if (!i.value) {
                        invalidParams.push(i.name)
                    }
                    return acc
                }, {})
                let errorMsg = 'Невозможно отправить форму!<br>' +
                    'Укажите '
                const localizedFields = {
                    name: "имя",
                    phone: "телефон",
                    question: "вопрос"
                }
                if (form.checkValidity()) {
                    form.classList.add("was-sent");
                    form.querySelector(".feedback__form-error").innerHTML = "";
                    form.querySelector(".btn").classList.remove("form-btn-invalid");
                    sendFormWithXhr(form)
                } else {
                    form.classList.remove("was-sent");
                    form.querySelector(".btn").classList.add("form-btn-invalid");
                    if (invalidParams[0] === "name") {
                        errorMsg += "свое "
                    } else {
                        errorMsg += "свой "
                    }
                    for (let i = 0; i < invalidParams.length; i++) {
                        errorMsg += localizedFields[invalidParams[i]]
                        if (i < invalidParams.length - 2) {
                            errorMsg += ", "
                        }
                        if (i === invalidParams.length - 2) {
                            errorMsg += " и "
                        }
                    }
                    errorMsg += "."
                    form.querySelector(".feedback__form-error").innerHTML = errorMsg;
                }
                form.classList.add("was-validated");
            },
            false
        );
        const successBtn = form.querySelector(".feedback__form-success").querySelector(".btn");
        successBtn.addEventListener('click', (event) => {
            event.preventDefault();
            form.classList.remove("was-validated");
            form.classList.remove("was-sent");
            form.reset();
        });
    });
}

//слайдер с кейсами
function initCasesSlider() {
    new Swiper('.cases-slider', {
        spaceBetween: 0,
        slidesPerView: 1,
        navigation: {
            nextEl: ".cases-slider-next",
            prevEl: ".cases-slider-prev",
        },
    });
}

// Слайдер с тарифами
function initTariffsSlider() {
    new Swiper('.tariffs-slider', {
        spaceBetween: 40,
        slidesPerView: 1,
        navigation: {
            nextEl: ".tariffs-slider-next",
            prevEl: ".tariffs-slider-prev",
        },
        breakpoints: {
            1200: {
                slidesPerView: 2,
                allowTouchMove: true,
            },
            1440: {
                slidesPerView: 4,
                allowTouchMove: false,
            }
        },
    });
}

//сертификаты
function initCertificated() {
    const lightbox = new PhotoSwipeLightbox({
        // may select multiple "galleries"
        gallery: '#certificates',

        // Elements within gallery (slides)
        children: 'a',

        // setup PhotoSwipe Core dynamic import
        pswpModule: PhotoSwipe,
    });
    lightbox.init();
}

//меню
function initMenu() {
    const menuBtn = document.querySelector('.navbar-toggler');
    menuBtn.addEventListener('click', (event) => {
        event.preventDefault();
        menuBtn.classList.toggle('navbar_expand');
    });
}

// Roles slider
function initRolesSlider() {
    const accordionItems = document.querySelectorAll(".accordion-item");
    let tempIdx;
    accordionItems.forEach(function (item, idx) {
        item.querySelector(".accordion-button").addEventListener("click", function () {
            tempIdx = idx;
            accordionItems.forEach(function (card, index) {
                card.querySelector(".accordion-button").classList.remove("top")
                card.querySelector(".accordion-button").classList.remove("bottom")
                if (index === tempIdx) {
                    item.classList.add("active")
                } else {
                    card.classList.remove("active")
                    if (index > tempIdx) {
                        card.querySelector(".accordion-button").classList.add("top")
                    } else {
                        card.querySelector(".accordion-button").classList.add("bottom")
                    }
                }
            });
        });
    })
}

function writeCounters() {
    document.write(`
    <script async src="https://www.googletagmanager.com/gtag/js?id=${GA_IDENTIFIER}"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            window.dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', "${GA_IDENTIFIER}");
    </script>
    
    <script type="text/javascript">
        (function (m, e, t, r, i, k, a) {
            m[i] = m[i] || function () {
                (m[i].a = m[i].a || []).push(arguments)
            };
            m[i].l = 1 * new Date();
            k = e.createElement(t), a = e.getElementsByTagName(t)[0], k.async = 1, k.src = r, a.parentNode.insertBefore(k, a)
        })
        (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

        ym("${YM_IDENTIFIER}", "init", {});
    </script>
    `)
}

writeCounters()

document.addEventListener('DOMContentLoaded', function () {
    initVersion()
    initForms()
    initRolesSlider()
    initMenu()
    initCertificated()
    initCasesSlider()
    initTariffsSlider()
})


