<?php

use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;

require_once "vendor/autoload.php";

$smtpHost = 'smtp.yandex.ru';
$smtpPort = 465;
$smtpLogin = 'robot@1creator.ru';
$smtpPassword = 'qwerty2H';
$smtpSecure = 'ssl';
$toMail = 'welcome@1creator.ru';
$mailSubject = 'Новая заявка с сайта netsimple';

$telegramBotToken = null;
$telegramChatId = null;


$message = "Имя: {$_POST["name"]}\r\n" .
    "Телефон: {$_POST["phone"]}\r\n" .
    "Вопрос: {$_POST["question"]}";


//telegram sending
if ($telegramBotToken) {
    try {
        $url = "https://api.telegram.org/bot${telegramBotToken}/sendMessage?chat_id=${telegramChatId}";
        $url = "{$url}&text=" . urlencode($message);
        $ch = curl_init();
        $optArray = array(
            CURLOPT_URL => $url,
            CURLOPT_SSLVERSION => 3,
            CURLOPT_RETURNTRANSFER => true
        );
        curl_setopt_array($ch, $optArray);
        $result = curl_exec($ch);
        if ($result == false) {
            throw new Exception(curl_error($ch), curl_errno($ch));
        }
        curl_close($ch);
    } catch (Exception $e) {
        return "fail send to telegram";
    }
}

//mail sending
if ($toMail) {
    //PHPMailer Object``
    $mail = new PHPMailer(true); //Argument true in constructor enables exceptions`
    $mail->isSMTP();
    $mail->SMTPDebug = 3;
    $mail->SMTPAuth = true;
    $mail->Username = $smtpLogin;
    $mail->Password = $smtpPassword;

    $mail->From = $smtpLogin;
    $mail->FromName = $smtpLogin;
    $mail->SMTPSecure = $smtpSecure;
    $mail->Port = $smtpPort;
    $mail->Host = $smtpHost;

    $mail->addAddress($toMail);

    $mail->Subject = $mailSubject;
    $mail->Body = $message;

    try {
        $mail->send();
    } catch (Exception $e) {
        return "fail send to email";
    }
}

return true;
